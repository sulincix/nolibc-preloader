#include <stdio.h>
#include <wchar.h>
char *__fgets_chk(char *s, size_t slen, int n, FILE *stream)
{
    return fgets(s, n, stream);
}

char *__fgets_unlocked_chk(char *s, size_t slen, int n, FILE *stream)
{
    return fgets_unlocked(s, n, stream);
}
size_t __fread_chk(void *buf, size_t buflen, size_t size, size_t nitems,
                   FILE *stream){
    return fread(buf, size, nitems, stream);
}
int __vasprintf_chk(char **strp, int flag, const char *format, va_list ap)
{
    return vasprintf(strp, format, ap);
}
int __vfprintf_chk(FILE *stream, int flag, const char *format, va_list ap)
{
    return vfprintf(stream, format, ap);
}
int __vprintf_chk(int flag, const char *format, va_list ap)
{
	return __vfprintf_chk(stdout, flag, format, ap);
}
int __vsnprintf_chk(char *s, size_t n, int flag, size_t slen,
                    const char *format, va_list ap)
{
    return vsnprintf(s, n, format, ap);
}
int __vsprintf_chk(char *s, int flag, size_t slen, const char *format,
                   va_list ap)
{
    return vsprintf(s, format, ap);
}
char *tmpnam_r(char *s)
{
    return tmpnam(s);
}

ssize_t
__read_chk (int fd, void *buf, size_t nbytes, size_t buflen)
{
    return read(fd, buf, nbytes);
}



size_t __mbrlen(const char *restrict s, size_t n, mbstate_t *restrict st) {
    return mbrlen(s, n, st);
}