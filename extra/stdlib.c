#include <stdarg.h>
#include <stddef.h>
#include <sys/syscall.h>
#include <string.h>
#include <wchar.h>
#include <string.h>

void *__memmove_chk (void *dest, const void *src,
              size_t len, size_t dest_len)
{
    return memmove(dest, src, len);
}

void *__memcpy_chk(void *dest, const void *src,
              size_t copy_amount, size_t dest_len)
{
    return memcpy(dest, src, copy_amount);
}

void *__memset_chk (void *dest, int c, size_t n, size_t dest_len)
{
    return memset(dest, c, n);
}



wchar_t *
__wmemmove_chk (wchar_t *s1, const wchar_t *s2, size_t n, size_t ns1)
{
  return (wchar_t *) memmove ((char *) s1, (char *) s2, n * sizeof (wchar_t));
}


int ioctl (int fd, unsigned long int req, ...)
{
	void *arg;
	va_list ap;
	va_start(ap, req);
	arg = va_arg(ap, void *);
	va_end(ap);
	return syscall(SYS_ioctl, fd, req, arg);
}

ssize_t getrandom(void *buf, size_t buflen, unsigned int flags){
    return 1;
}

