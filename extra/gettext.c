// replace setlocale with dummy function
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <dlfcn.h>

// glib fix
bool g_get_charset(char** charset){
    bool (*orig)(char**) = dlsym(RTLD_NEXT, "g_get_charset");
    orig(charset);
    return true;
}

