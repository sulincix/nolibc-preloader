#include <unistd.h>
#include <sys/param.h>
#ifdef __GLIBC__
ssize_t
__pread_chk (int fd, void *buf, size_t nbytes, off_t offset, size_t buflen)
{
  return 0;
}
ssize_t
__pread64_chk (int fd, void *buf, size_t nbytes, off_t offset, size_t buflen)
{
  return 0;
}
#endif
