LNX_SRC="https://raw.githubusercontent.com/torvalds/linux/master/tools/include/nolibc/"
DESTDIR=/
LIBDIR=/lib
all: build

nolibc:
	mkdir -p nolibc
	cat nolibc.list | sed "s|.*|wget -c -O nolibc/& $(LNX_SRC)&|g" | sh
	touch nolibc/nolibc.h nolibc/arch.h nolibc/sys.h

build: nolibc
	$(CC) $(wildcard extra/*.c) main.c -Inolibc -nostdlib -o nolibc.so -shared -fPIC -O0 -Wno-all -Wno-int-conversion

install:
	mkdir -p $(DESTDIR)/$(LIBDIR)/ $(DESTDIR)/etc/profile.d
	install nolibc.so $(DESTDIR)/$(LIBDIR)/
	echo 'export LD_PRELOAD="/$(LIBDIR)/nolibc.so $${LD_PRELOAD}"' > $(DESTDIR)/etc/profile.d/nolibc.sh

clean:
	rm -rf nolibc nolibc.so
