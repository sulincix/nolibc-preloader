#ifndef _NOLIBC_H
#define _NOLIBC_H
#define _SYS_TYPES_H

#define __uint32_t uint32_t
#define STDERR_FILENO 2
#define STDOUT_FILENO 1
#define SIGABRT 6
#define static

#include <asm/unistd.h>
#include <sys/ioctl.h>

#include "std.h"
#include "types.h"
#include "ctype.h"
#include "string.h"

/* Used by programs to avoid std includes */
#define NOLIBC
#endif
